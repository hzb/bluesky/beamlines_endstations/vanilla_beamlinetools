from .base import *
from .beamline import *
from bluesky.suspenders import SuspendWhenOutsideBand, SuspendWhenChanged


print('\n\nLOADING suspenders.py')

if 'next_injection' in devices_dictionary.keys():
    injection_suspender = SuspendWhenOutsideBand(next_injection.time_next_injection, band_bottom=2, band_top=298)

if 'v13' in devices_dictionary.keys():
    v13_suspender = SuspendWhenChanged(v13.status, expected_value=2, allow_resume=True, sleep=5)

if 'bs' in devices_dictionary.keys():
    bs_suspender = SuspendWhenChanged(bs.status, expected_value=0, allow_resume=True, sleep=5)

if 'v13' in devices_dictionary.keys() and 'bs' in devices_dictionary.keys():
    def check_beamshutter():
        # account for the case when no supenders are active
        if RE.suspenders == ():
            RE.install_suspender(v13_suspender)
            RE.install_suspender(bs_suspender)
            print("Suspender for beamshutter and v13 valve installed")
            return

        for sus in RE.suspenders:
            if sus == v13_suspender:
                RE.remove_suspender(v13_suspender)
                RE.remove_suspender(bs_suspender)
                print("Suspender for beamshutter and v13 valve removed")
                return
        RE.install_suspender(v13_suspender)
        RE.install_suspender(bs_suspender)
        print("Suspender for beamshutter and v13 valve installed")
        return
    print('check_beamshutter function is available')



