from .base import *
from .beamline import *
from .plans import *
from .tools import *
from .baseline import *
# from .authentication_and_metadata import *
from .magics import *
from .data_callbacks import *

# this block is deleting the functions, so that we can use the magics that have the same name
imported_objects = get_imported_objects('/opt/bluesky/beamlinetools/beamlinetools/beamline_config/plans.py')
plan_names = [obj for obj in imported_objects if not obj.startswith('_')]
for name in plan_names:
    exec(f'del {name}')