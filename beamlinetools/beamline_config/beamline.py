"""Module:      beamline.py

Description:    Instantiate beamline devices. Instatiation is based on device configuration y(a)ml file and rml file.
                Global variables are created to have direct access to instantiated devices using just instance names, e.g. v3.
                Access to instantiated devices by means of a dictionary "devices_dictionary" is always possible, e.g. devices_dictionary["v3"]
"""

# Import functions necessary for beamline device instantiation
from bessyii_devices_instantiation.beamline_devices import (  
    instantiate,
    wait_for_device_connection,
)

from bessyii.utils.helper import add_to_global_scope


print('\n\nLOADING beamline.py')


# Instantiate beamline devices
devices_dictionary: dict[object] = instantiate()

# Wait for instantiated devices to be connected
wait_for_device_connection(devices_dictionary)

# Create global variables from the "devices"
add_to_global_scope(devices_dictionary, globals())

# importing sim devices for testing
from ophyd.sim import det, noisy_det, motor
