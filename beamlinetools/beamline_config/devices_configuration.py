devices_dict = {
    "accelerator": {
        'axes' : {
                'next_injection': ['time_next_injection', 'time_last_injection'],

        },
        'device_type' : 'motor',
        'headers' : ['next_injection', 'last_injection']
    },
}